<?php

/**
 * 361GRAD Element Fullwidthimage
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_fullwidthimage'] = ['Bild in voller Breite', 'Full Width Image mit optionalem Badge Image.'];

$GLOBALS['TL_LANG']['tl_content']['dse_image']     = ['Bild', 'Hauptbilddatei'];
$GLOBALS['TL_LANG']['tl_content']['dse_imageSize'] = ['Bildgröße', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_badgeImage']     = ['Badge Image', 'Zusätzliche (Abzeichen-) Bilddatei'];
$GLOBALS['TL_LANG']['tl_content']['dse_badgeImageSize'] = ['', ''];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];
