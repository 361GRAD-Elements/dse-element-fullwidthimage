<?php

/**
 * 361GRAD Element Fullwidthimage
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_fullwidthimage'] = ['Fullwidth Image', 'Fullwidth Image with optional Badge Image.'];

$GLOBALS['TL_LANG']['tl_content']['dse_image']     = ['Image', 'Main image file'];
$GLOBALS['TL_LANG']['tl_content']['dse_imageSize'] = ['Image size', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_badgeImage']     = ['Badge Image', 'Additional (badge) image file'];
$GLOBALS['TL_LANG']['tl_content']['dse_badgeImageSize'] = ['Image size', ''];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];
